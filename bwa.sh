#!/bin/bash

rawdata1=$1
rawdata2=$2
sample_name=$3
ref=$4

echo -ne '####################                                                                                (20%)\r'
sleep 1
(time bwa index -p "$ref"bwaidx -a bwtsw /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/"$ref".fa) 2>>"$sample_name"_output.txt
echo -ne '########################                                                                            (24%)\r'
sleep 1
(time samtools faidx /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/"$ref".fa) 2>>"$sample_name"_output.txt

echo -ne '############################                                                                        (28%)\r'
sleep 1

line=$(head -n 1 $rawdata1)
IFS=':' read -r -a array <<< $line
ID="{@array[2]}.{@array[3]}"

(time /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/bwa-0.7.17/bwa mem -t 8 -R "@RG\tID:'$ID'\tSM:'$sample_name'\tPL:illumina\tLB:lib1" "$ref"bwaidx ./FastQC/"$sample_name"_trimmed_R1_paired.fastq.gz ./FastQC/"$sample_name"_trimmed_R2_paired.fastq.gz > "$sample_name"_aln.sam) 2>>"$sample_name"_output.txt

echo -ne '################################                                                                    (32%)\r'
sleep 1
(time samtools fixmate -O bam "$sample_name"_aln.sam "$sample_name"_aln.bam) 2>>"$sample_name"_output.txt

echo -ne '####################################                                                                (36%)\r'
sleep 1
(time java -jar /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/gatk-4.1.9.0/gatk CreateSequenceDictionary R=/mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/"$ref".fa O="$ref".dict) 2>>"$sample_name"_output.txt

echo -ne '########################################                                                            (40%)\r'
sleep 1
(time java -jar /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/gatk-4.1.9.0/gatk ValidateSamFile INPUT="$sample_name"_aln.bam MODE=SUMMARY) 2>>"$sample_name"_output.txt

echo -ne '############################################                                                        (44%)\r'
sleep 1
(time java -jar /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/gatk-4.1.9.0/gatk SortSam INPUT="$sample_name"_aln.bam OUTPUT="$sample_name"_sorted.bam SORT_ORDER=coordinate) 2>>"$sample_name"_output.txt # second bam

echo -ne '################################################                                                    (48%)\r'
sleep 1
(time java -jar /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/gatk-4.1.9.0/gatk MarkDuplicates INPUT="$sample_name"_sorted.bam OUTPUT="$sample_name"_dedup.bam METRICS_FILE="$sample_name".metrics) 2>>"$sample_name"_output.txt # third bam

echo -ne '####################################################                                                (52%)\r'
sleep 1
(time samtools index "$sample_name"_dedup.bam) 2>>"$sample_name"_output.txt

cp /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/dbsnp."$ref".vcf.bgz.tbi ./

echo -ne '##########################################################                                          (58%)\r'
sleep 1
(time /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/gatk-4.1.9.0/gatk BaseRecalibrator -I "$sample_name"_dedup.bam -R /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/"$ref".fa --known-sites /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/dbsnp."$ref".vcf.bgz -O recal_data.table) 2>>"$sample_name"_output.txt

echo -ne '################################################################                                    (64%)\r'
sleep 1
(time /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/gatk-4.1.9.0/gatk ApplyBQSR -R /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/"$ref".fa -I "$sample_name"_dedup.bam --bqsr-recal-file recal_data.table -O output_recal.bam) 2>>"$sample_name"_output.txt

echo -ne '######################################################################                              (70%)\r'
sleep 1
(time samtools index output_recal.bam) 2>>"$sample_name"_output.txt

echo -ne '############################################################################                        (76%)\r'
sleep 1
(time /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/gatk-4.1.9.0/gatk --java-options "-Xmx8g" HaplotypeCaller -R /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/"$ref".fa -I output_recal.bam -O output.g.vcf.gz -ERC GVCF) 2>>"$sample_name"_output.txt

echo -ne '####################################################################################                (84%)\r'
sleep 1
(time /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/gatk-4.1.9.0/gatk --java-options "-Xmx8g" GenotypeGVCFs -R /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/"$ref".fa -V output.g.vcf.gz -O "$sample_name"_output.vcf) 2>>"$sample_name"_output.txt

echo -ne '##########################################################################################          (90%)\r'
sleep 1
(time /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/gatk-4.1.9.0/gatk VariantRecalibrator -R /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/hs37d5.fa -V "$sample_name"_output.vcf --resource:dbsnp,known=true,training=true,truth=true,prior=2.0 /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/dbsnp."$ref".vcf.bgz -an QD -an MQ -an MQRankSum -an ReadPosRankSum -an FS -an SOR -mode BOTH -O output.recal --tranches-file output.tranches --rscript-file output.plots.R) 2>>"$sample_name"_output.txt

echo -ne '###############################################################################################     (95%)\r'
sleep 1
(time /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/gatk-4.1.9.0/gatk ApplyVQSR -R /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/hs37d5.fa -V "$sample_name"_output.vcf -O "$sample_name"_final_output.vcf.gz --recal-file output.recal -mode BOTH) 2>>"$sample_name"_output.txt

echo '####################################################################################################(100%)'
sleep 1

(time bcftools annotate -a ~/tools/dbsnp."$ref".vcf.bgz -c ID "$sample_name"_final_output.vcf.gz > "$sample_name"_final.vcf) 2>>"$sample_name"_output.txt

(time table_annovar.pl -vcfinput "$sample_name"_final.vcf ~/tools/annovar/humandb/ --outfile "$sample_name" -buildver hg19 --protocol refGene,ALL.sites.2014_10,gnomad_exome,clinvar_20210123,dbnsfp30a --operation g,f,f,f,f) 2>>"$sample_name"_output.txt

(time ~/tools/GATK\ 4.2/gatk VariantsToTable -V "$sample_name".hg19_multianno.vcf -O "$sample_name".table -F CHROM -F POS -F ID -F REF -F ALT -F QUAL -F FILTER -F NS -F DP -F DPB -F AC -F AN -F AF -F RO -F AO -F PRO -F PAO -F QR -F QA -F PQR -F PQA -F SRF -F SRR -F SAF -F SAR -F SRP -F SAP -F AB -F ABP -F RUN -F RPP -F RPPR -F RPL -F RPR -F EPP -F EPPR -F DPRA -F ODDS -F GTI -F TYPE -F CIGAR -F NUMALT -F MEANALT -F LEN -F MQM -F MQMR -F PAIRED -F PAIREDR -F ANNOVAR_DATE -F Func.refGene -F Gene.refGene -F GeneDetail.refGene -F ExonicFunc.refGene -F AAChange.refGene -F ALL.sites.2014_10 -F gnomAD_exome_ALL -F gnomAD_exome_AFR -F gnomAD_exome_AMR -F gnomAD_exome_ASJ -F gnomAD_exome_EAS -F gnomAD_exome_FIN -F gnomAD_exome_NFE -F gnomAD_exome_OTH -F gnomAD_exome_SAS -F CLNALLELEID -F CLNDN -F CLNDISDB -F CLNREVSTAT -F CLNSIG -F SIFT_score -F SIFT_pred -F Polyphen2_HDIV_score -F Polyphen2_HDIV_pred -F Polyphen2_HVAR_score -F Polyphen2_HVAR_pred -F LRT_score -F LRT_pred -F MutationTaster_score -F MutationTaster_pred -F MutationAssessor_score -F MutationAssessor_pred -F FATHMM_score -F FATHMM_pred -F PROVEAN_score -F PROVEAN_pred -F VEST3_score -F CADD_raw -F CADD_phred -F DANN_score -F fathmm-MKL_coding_score -F fathmm-MKL_coding_pred -F MetaSVM_score -F MetaSVM_pred -F MetaLR_score -F MetaLR_pred -F integrated_fitCons_score -F integrated_confidence_value -F GERP++_RS -F phyloP7way_vertebrate -F phyloP20way_mammalian -F phastCons7way_vertebrate -F phastCons20way_mammalian -F SiPhy_29way_logOdds -F ALLELE_END) 2>>"$sample_name"_output.txt

mv "$sample_name".table "$sample_name".txt
sed -i 's/,/;/g' "$sample_name".txt
sed -i 's/	/,/g' "$sample_name".txt
mv "$sample_name".txt ./VCF/"$sample_name"_ann_vcf.csv

mv "$sample_name"_final.vcf ./VCF

rm hg* "$sample_name"_aln.sam "$sample_name"_aln.bam "$sample_name"_sorted.bam "$sample_name"_dedup.bam recal_data.table output_recal.bam output.g.vcf.gz "$sample_name"_output.vcf "$sample_name"_final_output.vcf.gz 

echo "The pipeline is done!"
perl /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/time.pl "$sample_name"_output.txt