#!/bin/bash

vcf=$1
sample_name=$2

arr=("$@")
#echo $@
#
# if [ "${#arr[@]}" -gt 1 ]
# then
#echo ${arr[@]}
arr=("${arr[@]:1}") # remove vcf
arr=("${arr[@]:1}") # remove sample_name
# now left with the csv file, just replace the spaces with '\ '

csv=$(printf " %s" "${arr[@]}")
#csv="${csv:1}"
csv="${csv:1}"
# function join { local IFS="$1"; shift; echo "$*"; }
# join '%s' ${arr[@]}
# fi

if [ ! -z "$csv" ]
then

head -n 1 "$vcf" > ./Gene_panel/"$sample_name"_"$csv"

while IFS=$'\n\r' read -r line; do

grep $line "$vcf" >> ./Gene_panel/"$sample_name"_"$csv"

done < "$csv"

#echo "The SNPs specific to the disease $csv have been written to '$sample_name'_'$csv'.csv"
fi