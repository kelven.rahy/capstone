#!/bin/bash

rawdata1=$1
rawdata2=$2
sample_name=$3

echo "Pipeline running for the files $rawdata1 and $rawdata2! All output will be written to "$sample_name"_output.txt"

# Quality control:
# echo -ne '                                                                                                    (0%)\r'
# sleep 1
# (time /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/FastQC/fastqc --outdir=./ "$rawdata1") 2>"$sample_name"_output.txt
# echo -ne '####                                                                                                (4%)\r'
# sleep 1
# (time /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/FastQC/fastqc --outdir=./ "$rawdata2") 2>>"$sample_name"_output.txt


for filename in /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/Trimmomatic-0.39/adapters/*PE*; do
	file=$filename
	while IFS= read line
	do
		IFS= read line
			if(zcat "$rawdata1" | head -n 20000 | grep $line >> res.txt); then
				adapter_filename=$filename
				break
			fi
	done < "$file"
done
rm res.txt

# Trimming:
echo -ne '########                                                                                            (8%)\r'
sleep 1
(time java -jar /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/Trimmomatic-0.39/trimmomatic-0.39.jar PE -threads 8 \-trimlog ./"$sample_name".log "$rawdata1" "$rawdata2" ./"$sample_name"_trimmed_R1_paired.fastq.gz ./"$sample_name"_trimmed_R1_unpaired.fastq.gz ./"$sample_name"_trimmed_R2_paired.fastq.gz ./"$sample_name"_trimmed_R2_unpaired.fastq.gz ILLUMINACLIP:"$file":2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36) 2>>"$sample_name"_output.txt


# Quality control after trimming:
echo -ne '############                                                                                        (12%)\r'
sleep 1
(time /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/FastQC/fastqc "$sample_name"_trimmed_R1_paired.fastq.gz) 2>>"$sample_name"_output.txt
echo -ne '################                                                                                    (16%)\r'
sleep 1
(time /mnt/c/Users/ooo/Desktop/k/Uni/Fall\ 2020/Functional\ Genomics/Tools/FastQC/fastqc "$sample_name"_trimmed_R2_paired.fastq.gz) 2>>"$sample_name"_output.txt

unzip "$sample_name"_trimmed_R1_paired.fastq.gz

mv "$sample_name"_trimmed_R1_unpaired.fastq.gz ./FastQC
mv "$sample_name"_trimmed_R2_paired.fastq.gz ./FastQC