#!/bin/bash

# wget ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/technical/reference/phase2_reference_assembly_sequence/hs37d5.fa.gz
# path: /mnt/gkhazen/RizkWES/Feb2021/RAW-20D0001_S53_R1_001.fastq.gz

echo "Please enter the name of the first fastq file:"
read rawdata1

while [ ! -e "$rawdata1" ]
do
	echo "The file '$rawdata1' was not found! Please enter again:"
	read rawdata1
done

echo "Please enter the name of the second fastq file:"
read rawdata2

while [ ! -e "$rawdata2" ]
do
	echo "The file '$rawdata2' was not found! Please enter again:"
	read rawdata2
done

echo "Which GATK version would you like to use? Available versions: 3.3, 3.7, and 4.1.9"
read GATK
found=$(ls ~/tools | grep "GATK $GATK\$")

while [ -z "$found" ]
do
	echo "GATK version not found! Please enter a valid version. Available versions: 3.3, 3.7, and 4.1.9"
	read GATK
	found=$(ls ~/tools | grep "GATK $GATK\$")
done

# Quality control:
echo -ne '                                                                                                    (0%)\r'
sleep 1
(time ~/tools/FastQC/fastqc --outdir=./ "$rawdata1") 2>time.txt
echo -ne '####                                                                                                (4%)\r'
sleep 1
(time ~/tools/FastQC/fastqc --outdir=./ "$rawdata2") 2>>time.txt


for filename in ~/tools/Trimmomatic-0.39/adapters/*PE*; do
	file=$filename
	while IFS= read line
	do
		IFS= read line
			if(zcat "$rawdata1" | head -n 20000 | grep $line >> res.txt); then
				adapter_filename=$filename
				break
			fi
	done < "$file"
done
rm res.txt

sample_name="20D0001MP"

# Trimming:
echo -ne '########                                                                                            (8%)\r'
sleep 1
(time java -jar ~/tools/Trimmomatic-0.39/trimmomatic-0.39.jar PE -threads 8 \-trimlog ./"$sample_name".log "$rawdata1" "$rawdata2" ./trimmed_R1_paired.fastq.gz ./trimmed_R1_unpaired.fastq.gz ./trimmed_R2_paired.fastq.gz ./trimmed_R2_unpaired.fastq.gz ILLUMINACLIP:$file:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36) 2>>time.txt

# Quality control after trimming:
echo -ne '############                                                                                        (12%)\r'
sleep 1
(time ~/tools/FastQC/fastqc trimmed_R1_paired.fastq.gz) 2>>time.txt
echo -ne '################                                                                                    (16%)\r'
sleep 1
(time ~/tools/FastQC/fastqc trimmed_R2_paired.fastq.gz) 2>>time.txt

echo -ne '####################                                                                                (20%)\r'
sleep 1
(time bwa index -p hs37d5bwaidx -a bwtsw ~/tools/hs37d5.fa) 2>>time.txt
echo -ne '########################                                                                            (24%)\r'
sleep 1
(time samtools faidx ~/tools/hs37d5.fa) 2>>time.txt

echo -ne '############################                                                                        (28%)\r'
sleep 1
(time ~/tools/bwa-0.7.17/bwa mem -t 8 -R "@RG\tID:1_62733841_S132\tSM:62733841_S132\tPL:illumina\tLB:lib1\tPU:HNLTFDMXX.1.GTTAGATACC+TTACGGAGTC" hs37d5bwaidx trimmed_R1_paired.fastq.gz trimmed_R2_paired.fastq.gz > "$sample_name"_aln.sam) 2>>time.txt

echo -ne '################################                                                                    (32%)\r'
sleep 1
(time samtools fixmate -O bam "$sample_name"_aln.sam "$sample_name"_aln.bam) 2>>time.txt

echo "Currently creating sequence dictionary"
echo -ne '####################################                                                                (36%)\r'
sleep 1
(time java -jar ~/tools/picard/build/libs/picard.jar CreateSequenceDictionary R=~/tools/hs37d5.fa O=hs37d5.dict) 2>>time.txt

echo "Currently validating bam file"
echo -ne '########################################                                                            (40%)\r'
sleep 1
(time java -jar ~/tools/picard/build/libs/picard.jar ValidateSamFile INPUT="$sample_name"_aln.bam MODE=SUMMARY) 2>>time.txt

echo "Currently sorting the bam file"
echo -ne '############################################                                                        (44%)\r'
sleep 1
(time java -jar ~/tools/picard/build/libs/picard.jar SortSam INPUT="$sample_name"_aln.bam OUTPUT="$sample_name"_sorted.bam SORT_ORDER=coordinate) 2>>time.txt # second bam

echo "Currently marking duplicates"
echo -ne '################################################                                                    (48%)\r'
sleep 1
(time java -jar ~/tools/picard/build/libs/picard.jar MarkDuplicates INPUT="$sample_name"_sorted.bam OUTPUT="$sample_name"_dedup.bam METRICS_FILE="$sample_name".metrics) 2>>time.txt # third bam

echo -ne '####################################################                                                (52%)\r'
sleep 1
(time samtools index "$sample_name"_dedup.bam) 2>>time.txt

cp ~/tools/Mills_and_1000G_gold_standard.indels.hs37d5.sites.vcf.bgz.tbi ./
#cp ~/tools/hs37d5-db142-v1.tbi ./

wget ftp://anonymous@share.sph.umich.edu/gotcloud/ref/hs37d5-db142-v1.tgz
tar xzf hs37d5-db142-v1.tgz
rm -f hs37d5-db142-v1.tgz


if [ "$GATK" = "3.7" -o "$GATK" = "3.3" ]
then
echo -ne '########################################################                                            (56%)\r'
sleep 1
(time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T RealignerTargetCreator -R ~/tools/hs37d5.fa -known ~/tools/Mills_and_1000G_gold_standard.indels.hs37d5.sites.vcf.bgz -I "$sample_name"_dedup.bam -o "$sample_name"_targetcreator.intervals) 2>>time.txt

echo -ne '############################################################                                        (60%)\r'
sleep 1
(time java -Xmx8G -Djava.io.tmpdir=/tmp -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T IndelRealigner -R ~/tools/hs37d5.fa -targetIntervals "$sample_name"_targetcreator.intervals  -known ~/tools/Mills_and_1000G_gold_standard.indels.hs37d5.sites.vcf.bgz -I "$sample_name"_dedup.bam -o "$sample_name"_indelreadligner.bam) 2>>time.txt # fourth bam

echo -ne '################################################################                                    (64%)\r'
sleep 1
(time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T BaseRecalibrator -R ~/tools/hs37d5.fa -I "$sample_name"_indelreadligner.bam -knownSites ~/tools/hs37d5-db142-v1 -o recal.table) 2>>time.txt

echo -ne '######################################################################                              (70%)\r'
sleep 1
(time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T PrintReads -R ~/tools/hs37d5.fa -I "$sample_name"_indelreadligner.bam -BQSR recal.table -o "$sample_name"_final.bam) 2>>time.txt # fifth bam

echo -ne '##########################################################################                          (74%)\r'
sleep 1
(time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T BaseRecalibrator -R ~/tools/hs37d5.fa -I "$sample_name"_indelreadligner.bam -knownSites ~/tools/hs37d5-db142-v1 -BQSR recal.table -o "$sample_name"-secondpass.table) 2>>time.txt

echo -ne '##############################################################################                      (78%)\r'
sleep 1
(time samtools index "$sample_name"_final.bam) 2>>time.txt

echo -ne '################################################################################                    (80%)\r'
sleep 1
(time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T HaplotypeCaller -R ~/tools/hs37d5.fa -I "$sample_name"_final.bam -o "$sample_name".g.vcf -ERC GVCF --variant_index_type LINEAR --variant_index_parameter 128000) 2>>time.txt

echo -ne '######################################################################################              (86%)\r'
sleep 1
(time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T GenotypeGVCFs -R ~/tools/hs37d5.fa -V "$sample_name".g.vcf -o "$sample_name"_output.vcf) 2>>time.txt

echo -ne '############################################################################################        (92%)\r'
sleep 1
#(time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T VariantFiltration -R ~/tools/hs37d5.fa  -V "$sample_name"_output.vcf  -o "$sample_name"_filtered.vcf.gz --filterName "FS" --filterExpression "FS>60.0" --filterName "MQRankSum" --filterExpression "MQRankSum<-12.5" --filterName "QD" --filterExpression "QD<2.0" --filterName "MQ" --filterExpression "MQ<40.0" --filterName "ReadPosRankSum" --filterExpression "ReadPosRankSum<-8.0" --filterName "HaplotypeScore" --filterExpression "HaplotypeScore>13") 2>>time.txt

echo -ne '###############################################################################################     (95%)\r'
sleep 1
#(time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T SelectVariants -R ~/tools/hs37d5.fa -V "$sample_name"_filtered.vcf.gz -o "$sample_name"_filteredExcluded_SNP.vcf.gz --excludeFiltered) 2>>time.txt

else
echo -ne '##########################################################                                          (58%)\r'
sleep 1
(time ~/tools/GATK\ $GATK/gatk BaseRecalibrator -I "$sample_name"_dedup.bam -R ~/tools/hs37d5.fa --known-sites ~/tools/hs37d5-db142-v1 -O recal_data.table) 2>>time.txt

echo -ne '################################################################                                    (64%)\r'
sleep 1
(time ~/tools/GATK\ $GATK/gatk ApplyBQSR -R ~/tools/hs37d5.fa -I "$sample_name"_dedup.bam --bqsr-recal-file recal_data.table -O output_recal.bam) 2>>time.txt

echo -ne '######################################################################                              (70%)\r'
sleep 1
(time samtools index output_recal.bam) 2>>time.txt

echo -ne '############################################################################                        (76%)\r'
sleep 1
(time ~/tools/GATK\ $GATK/gatk --java-options "-Xmx8g" HaplotypeCaller -R ~/tools/hs37d5.fa -I output_recal.bam -O output.g.vcf.gz -ERC GVCF) 2>>time.txt

echo -ne '####################################################################################                (84%)\r'
sleep 1
(time ~/tools/GATK\ $GATK/gatk --java-options "-Xmx8g" GenotypeGVCFs -R ~/tools/hs37d5.fa -V output.g.vcf.gz -O "$sample_name"_4_output.vcf) 2>>time.txt

echo -ne '##########################################################################################          (90%)\r'
sleep 1
#(time ~/tools/GATK\ $GATK/gatk VariantFiltration -R ~/tools/hs37d5.fa  -V "$sample_name"_4_output.vcf -O "$sample_name"_filtered.vcf.gz --filter-name "FS" --filter-expression "FS>60.0" --filter-name "MQRankSum" --filter-expression "MQRankSum<-12.5" --filter-name "QD" --filter-expression "QD<2.0" --filter-name "MQ" --filter-expression "MQ<40.0" --filter-name "ReadPosRankSum" --filter-expression "ReadPosRankSum<-8.0" --filter-name "HaplotypeScore" --filter-expression "HaplotypeScore>13") 2>>time.txt

echo -ne '###############################################################################################     (95%)\r'
sleep 1
#(time ~/tools/GATK\ $GATK/gatk SelectVariants -R ~/tools/hs37d5.fa -V "$sample_name"_4_filtered.vcf.gz -O "$sample_name"_filteredExcluded_SNP.vcf.gz --exclude-filtered) 2>>time.txt
fi

# (time java -Xmx4g -jar ~/tools/snpEff/snpEff.jar ann -v -stats "$sample_name"_stats.html -csvStats "$sample_name"_SNP_stats.csv hs37d5 "$sample_name"_filteredExcluded_SNP.vcf > "$sample_name"_SNP_ann.vcf) 2>>time.txt

# (time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T VariantAnnotator -R ~/tools/hs37d5.fa -A FisherStrand -A ReadPosRankSumTest -A MappingQualityRankSumTest -A MappingQualityZero -XA SpanningDeletions -XA TandemRepeatAnnotator -I "$sample_name"_final.bam -V "$sample_name"_SNP_ann.vcf -o annotated.vcf) 2>>time.txt

# ~/tools/tabix-0.2.6/bgzip -c  "$sample_name"_SNP_ann.vcf > "$sample_name"_SNP_ann.vcf.bgz) 2>>time.txt
# bcftools index -t "$sample_name"_SNP_ann.vcf.bgz

echo -ne '#################################################################################################   (97%)\r'
sleep 1
(time bcftools annotate -a ~/tools/hs37d5-db142-v1 -c ID "$sample_name"_filtered.vcf.gz > "$sample_name"_final.vcf) 2>>time.txt

echo '####################################################################################################(100%)'
sleep 1

echo "The pipeline is done!"
perl ~/tools/time.pl time.txt