#!/bin/bash

rawdata1=$1
rawdata2=$2
GATK=$3
sample_name=$4
vcf_loc=$5

# Quality control:
echo -ne '                                                                                                    (0%)\r'
sleep 1
(time ~/tools/FastQC/fastqc --outdir=./ "$rawdata1") 2>time.txt
echo -ne '####                                                                                                (4%)\r'
sleep 1
(time ~/tools/FastQC/fastqc --outdir=./ "$rawdata2") 2>>time.txt


for filename in ~/tools/Trimmomatic-0.39/adapters/*PE*; do
	file=$filename
	while IFS= read line
	do
		IFS= read line
			if(zcat "$rawdata1" | head -n 20000 | grep $line >> res.txt); then
				adapter_filename=$filename
				break
			fi
	done < "$file"
done
rm res.txt


# Trimming:
echo -ne '########                                                                                            (8%)\r'
sleep 1
(time java -jar ~/tools/Trimmomatic-0.39/trimmomatic-0.39.jar PE -threads 8 \-trimlog ./"$sample_name".log "$rawdata1" "$rawdata2" ./trimmed_R1_paired.fastq.gz ./trimmed_R1_unpaired.fastq.gz ./trimmed_R2_paired.fastq.gz ./trimmed_R2_unpaired.fastq.gz ILLUMINACLIP:$file:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36) 2>>time.txt

# Quality control after trimming:
echo -ne '############                                                                                        (12%)\r'
sleep 1
(time ~/tools/FastQC/fastqc trimmed_R1_paired.fastq.gz) 2>>time.txt
echo -ne '################                                                                                    (16%)\r'
sleep 1
(time ~/tools/FastQC/fastqc trimmed_R2_paired.fastq.gz) 2>>time.txt

echo -ne '####################                                                                                (20%)\r'
sleep 1
(time bwa index -p hg19bwaidx -a bwtsw ~/tools/hg19.fa) 2>>time.txt
echo -ne '########################                                                                            (24%)\r'
sleep 1
(time samtools faidx ~/tools/hg19.fa) 2>>time.txt

echo -ne '############################                                                                        (28%)\r'
sleep 1
(time ~/tools/bwa-0.7.17/bwa mem -t 8 -R "@RG\tID:1_62733841_S132\tSM:62733841_S132\tPL:illumina\tLB:lib1\tPU:HNLTFDMXX.1.GTTAGATACC+TTACGGAGTC" hg19bwaidx trimmed_R1_paired.fastq.gz trimmed_R2_paired.fastq.gz > "$sample_name"_aln.sam) 2>>time.txt

echo -ne '################################                                                                    (32%)\r'
sleep 1
(time samtools fixmate -O bam "$sample_name"_aln.sam "$sample_name"_aln.bam) 2>>time.txt

if [ "$GATK" = "3.7" -o "$GATK" = "3.3" ]
then
echo "Currently creating sequence dictionary"
echo -ne '####################################                                                                (36%)\r'
sleep 1
(time java -jar ~/tools/picard/build/libs/picard.jar CreateSequenceDictionary R=~/tools/hg19.fa O=hg19.dict) 2>>time.txt

echo "Currently validating bam file"
echo -ne '########################################                                                            (40%)\r'
sleep 1
(time java -jar ~/tools/picard/build/libs/picard.jar ValidateSamFile INPUT="$sample_name"_aln.bam MODE=SUMMARY) 2>>time.txt

echo "Currently sorting the bam file"
echo -ne '############################################                                                        (44%)\r'
sleep 1
(time java -jar ~/tools/picard/build/libs/picard.jar SortSam INPUT="$sample_name"_aln.bam OUTPUT="$sample_name"_sorted.bam TMP_DIR=./ SORT_ORDER=coordinate) 2>>time.txt # second bam

echo "Currently marking duplicates"
echo -ne '################################################                                                    (48%)\r'
sleep 1
(time java -jar ~/tools/picard/build/libs/picard.jar MarkDuplicates INPUT="$sample_name"_sorted.bam OUTPUT="$sample_name"_dedup.bam METRICS_FILE="$sample_name".metrics) 2>>time.txt # third bam

echo -ne '####################################################                                                (52%)\r'
sleep 1
(time samtools index "$sample_name"_dedup.bam) 2>>time.txt

cp ~/tools/Mills_and_1000G_gold_standard.indels.hg19.sites.vcf.bgz.tbi ./
cp ~/tools/dbsnp_138.hg19.vcf.bgz.tbi ./


echo -ne '########################################################                                            (56%)\r'
sleep 1
(time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T RealignerTargetCreator -R ~/tools/hg19.fa -known ~/tools/Mills_and_1000G_gold_standard.indels.hg19.sites.vcf.bgz -I "$sample_name"_dedup.bam -o "$sample_name"_targetcreator.intervals) 2>>time.txt

echo -ne '############################################################                                        (60%)\r'
sleep 1
(time java -Xmx8G -Djava.io.tmpdir=/tmp -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T IndelRealigner -R ~/tools/hg19.fa -targetIntervals "$sample_name"_targetcreator.intervals  -known ~/tools/Mills_and_1000G_gold_standard.indels.hg19.sites.vcf.bgz -I "$sample_name"_dedup.bam -o "$sample_name"_indelreadligner.bam) 2>>time.txt # fourth bam

echo -ne '################################################################                                    (64%)\r'
sleep 1
(time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T BaseRecalibrator -R ~/tools/hg19.fa -I "$sample_name"_indelreadligner.bam -knownSites ~/tools/dbsnp_138.hg19.vcf.bgz -o recal.table) 2>>time.txt

echo -ne '######################################################################                              (70%)\r'
sleep 1
(time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T PrintReads -R ~/tools/hg19.fa -I "$sample_name"_indelreadligner.bam -BQSR recal.table -o "$sample_name"_final.bam) 2>>time.txt # fifth bam

echo -ne '##########################################################################                          (74%)\r'
sleep 1
(time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T BaseRecalibrator -R ~/tools/hg19.fa -I "$sample_name"_indelreadligner.bam -knownSites ~/tools/dbsnp_138.hg19.vcf.bgz -BQSR recal.table -o "$sample_name"-secondpass.table) 2>>time.txt

echo -ne '##############################################################################                      (78%)\r'
sleep 1
(time samtools index "$sample_name"_final.bam) 2>>time.txt

echo -ne '################################################################################                    (80%)\r'
sleep 1
(time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T HaplotypeCaller -R ~/tools/hg19.fa -I "$sample_name"_final.bam -o "$sample_name".g.vcf -ERC GVCF --variant_index_type LINEAR --variant_index_parameter 128000) 2>>time.txt

echo -ne '######################################################################################              (86%)\r'
sleep 1
(time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T GenotypeGVCFs -R ~/tools/hg19.fa -V "$sample_name".g.vcf -o "$sample_name"_output.vcf) 2>>time.txt

echo -ne '############################################################################################        (92%)\r'
sleep 1
(time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T VariantFiltration -R ~/tools/hg19.fa  -V "$sample_name"_output.vcf  -o "$sample_name"_filtered.vcf.gz --filterName "highFS" --filterExpression "FS>200.0" --filterName "lowMQRankSum" --filterExpression "MQRankSum<-15" --filterName "lowQD" --filterExpression "QD<2.0" --filterName "lowReadPosRankSum-20" --filterExpression "ReadPosRankSum<-20" --filterName "lowReadPosRankSum-10" --filterExpression "ReadPosRankSum<-10" --filterName "lowDP" --filterExpression "DP<10") 2>>time.txt

echo -ne '###############################################################################################     (95%)\r'
sleep 1
#(time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T SelectVariants -R ~/tools/hg19.fa -V "$sample_name"_filtered.vcf.gz -o "$sample_name"_filteredExcluded_SNP.vcf.gz --excludeFiltered) 2>>time.txt

# removing big files: 
rm hg* "$sample_name"_aln.sam "$sample_name"_aln.bam "$sample_name"_sorted.bam "$sample_name"_dedup.bam "$sample_name"_targetcreator.intervals "$sample_name"_indelreadligner.bam recal.table "$sample_name"-secondpass.table "$sample_name".g.vcf

else
echo "Currently creating sequence dictionary"
echo -ne '####################################                                                                (36%)\r'
sleep 1
(time ~/tools/GATK\ $GATK/gatk CreateSequenceDictionary R=~/tools/hg19.fa O=hg19.dict) 2>>time.txt

echo "Currently validating bam file"
echo -ne '########################################                                                            (40%)\r'
sleep 1
(time ~/tools/GATK\ $GATK/gatk ValidateSamFile INPUT="$sample_name"_aln.bam MODE=SUMMARY) 2>>time.txt

echo "Currently sorting the bam file"
echo -ne '############################################                                                        (44%)\r'
sleep 1
(time ~/tools/GATK\ $GATK/gatk SortSam INPUT="$sample_name"_aln.bam OUTPUT="$sample_name"_sorted.bam TMP_DIR=./ SORT_ORDER=coordinate) 2>>time.txt # second bam

echo "Currently marking duplicates"
echo -ne '################################################                                                    (48%)\r'
sleep 1
(time ~/tools/GATK\ $GATK/gatk MarkDuplicates INPUT="$sample_name"_sorted.bam OUTPUT="$sample_name"_dedup.bam METRICS_FILE="$sample_name".metrics) 2>>time.txt # third bam

echo -ne '####################################################                                                (52%)\r'
sleep 1
(time samtools index "$sample_name"_dedup.bam) 2>>time.txt

cp ~/tools/Mills_and_1000G_gold_standard.indels.hg19.sites.vcf.bgz.tbi ./
cp ~/tools/dbsnp_138.hg19.vcf.bgz.tbi ./

echo -ne '##########################################################                                          (58%)\r'
sleep 1
(time ~/tools/GATK\ $GATK/gatk BaseRecalibrator -I "$sample_name"_dedup.bam -R ~/tools/hg19.fa --known-sites ~/tools/dbsnp_138.hg19.vcf.bgz -O recal_data.table) 2>>time.txt

echo -ne '################################################################                                    (64%)\r'
sleep 1
(time ~/tools/GATK\ $GATK/gatk ApplyBQSR -R ~/tools/hg19.fa -I "$sample_name"_dedup.bam --bqsr-recal-file recal_data.table -O output_recal.bam) 2>>time.txt

echo -ne '######################################################################                              (70%)\r'
sleep 1
(time samtools index output_recal.bam) 2>>time.txt

echo -ne '############################################################################                        (76%)\r'
sleep 1
(time ~/tools/GATK\ $GATK/gatk --java-options "-Xmx8g" HaplotypeCaller -R ~/tools/hg19.fa -I output_recal.bam -O output.g.vcf.gz -ERC GVCF) 2>>time.txt

echo -ne '####################################################################################                (84%)\r'
sleep 1
(time ~/tools/GATK\ $GATK/gatk --java-options "-Xmx8g" GenotypeGVCFs -R ~/tools/hg19.fa -V output.g.vcf.gz -O "$sample_name"_4_output.vcf) 2>>time.txt

(time ~/tools/GATK\ $GATK/gatk ValidateVariants -R ~/tools/hg19.fa -V "$sample_name"_4_output.vcf --dbsnp ~/tools/dbsnp_138.hg19.vcf.bgz) 2>>time.txt


echo -ne '##########################################################################################          (90%)\r'
sleep 1
(time ~/tools/GATK\ $GATK/gatk VariantFiltration -R ~/tools/hg19.fa  -V "$sample_name"_4_output.vcf -O "$sample_name"_filtered.vcf.gz --filter-name "FS" --filter-expression "FS>200.0" --filter-name "MQRankSum" --filter-expression "MQRankSum<-15.0" --filter-name "QD" --filter-expression "QD<2.0" --filter-name "ReadPosRankSum" --filter-expression "ReadPosRankSum<-20.0" --filter-name "ReadPosRankSum" --filter-expression "ReadPosRankSum<-10.0" --filter-name "DP" --filter-expression "DP<10.0") 2>>time.txt

echo -ne '###############################################################################################     (95%)\r'
sleep 1
#(time ~/tools/GATK\ $GATK/gatk SelectVariants -R ~/tools/hg19.fa -V "$sample_name"_4_filtered.vcf.gz -O "$sample_name"_filteredExcluded_SNP.vcf.gz --exclude-filtered) 2>>time.txt

# removing big files: 
rm hg* "$sample_name"_aln.sam "$sample_name"_aln.bam "$sample_name"_sorted.bam "$sample_name"_dedup.bam recal_data.table output_recal.bam output.g.vcf.gz

fi

# (time java -Xmx4g -jar ~/tools/snpEff/snpEff.jar ann -v -stats "$sample_name"_stats.html -csvStats "$sample_name"_SNP_stats.csv hg19 "$sample_name"_filteredExcluded_SNP.vcf > "$sample_name"_SNP_ann.vcf) 2>>time.txt

# (time java -jar ~/tools/GATK\ $GATK/GenomeAnalysisTK.jar -T VariantAnnotator -R ~/tools/hg19.fa -A FisherStrand -A ReadPosRankSumTest -A MappingQualityRankSumTest -A MappingQualityZero -XA SpanningDeletions -XA TandemRepeatAnnotator -I "$sample_name"_final.bam -V "$sample_name"_SNP_ann.vcf -o annotated.vcf) 2>>time.txt

# ~/tools/tabix-0.2.6/bgzip -c  "$sample_name"_SNP_ann.vcf > "$sample_name"_SNP_ann.vcf.bgz) 2>>time.txt
# bcftools index -t "$sample_name"_SNP_ann.vcf.bgz

echo -ne '#################################################################################################   (97%)\r'
sleep 1
(time bcftools annotate -a ~/tools/dbsnp_138.hg19.vcf.bgz -c ID "$sample_name"_filtered.vcf.gz > "$sample_name"_"$GATK"_final.vcf) 2>>time.txt

echo '####################################################################################################(100%)'
sleep 1

echo "The pipeline is done!"

echo "$sample_name" >> ~/tools/info.txt
echo "$GATK" >> ~/tools/info.txt
rm m.txt
rm r.txt

grep -P "chr([0-9]|X|Y|M)*\t" "$vcf_loc" | awk '{print $3}' > rizk.txt
grep -P "chr([0-9]|X|Y|M)*\t" "$sample_name"_"$GATK"_final.vcf | awk '{print $3}' > mine.txt
sort mine.txt>m.txt
sort rizk.txt>r.txt
echo -n "SNPs in common:" >> ~/tools/info.txt
echo $(comm -12 r.txt m.txt | wc -l) >> ~/tools/info.txt
echo -n "SNPs found in Rizk but not mine:" >> ~/tools/info.txt
echo $(comm -23 r.txt m.txt | wc -l) >> ~/tools/info.txt

echo $(perl ~/tools/time.pl time.txt) >> ~/tools/info.txt