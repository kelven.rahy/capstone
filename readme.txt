Files:
- shiny(dashboard).R: the code for the shiny interface
- fasta.sh: Runs trimming and generates FastQC html
- bwa.sh: the automated code for running the GATK pipeline, generating an annotated VCF
- csv.sh: code that takes the VCF file and the gene panel(s) and finds the genes in common.